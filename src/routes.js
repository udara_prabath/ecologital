import React from 'react'

const Users = React.lazy(() => import('./pages/users'))
const SelectedUser = React.lazy(() => import('./pages/users/selectedUser'))
const Profile = React.lazy(() => import('./pages/users/profile'))

const common = [
    {path: '/', exact: true, name: 'Home'},
    {path: '/profile', name: 'Profile', exact: true, element: Profile},
]

const routes = [
    ...common,
    {path: '/users', name: 'Users', exact: true, element: Users},
    {path: '/users/:userId', name: 'View user', element: SelectedUser},
]

export const routesUser = [
    ...common
]

export default routes
