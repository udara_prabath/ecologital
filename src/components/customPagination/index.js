import ReactPaginate from 'react-paginate'
import {Col, Row} from "reactstrap"
import React from "react"
import './_pagination.scss'

export default (props) => (
    <div>
        <div className={'hr'}/>
        <Row className={'pagination-row'}>
            <Col className={'pagination-left col-md-6'}>
                    <span
                        className={'pagination-details'}>Showing {props.offset + 1} to {props.offset + props.numberOfElements} of {props.totalElements} entries</span>
            </Col>
            <Col className={'col-md-6'}>
                <ReactPaginate
                    previousLabel={''}
                    nextLabel={''}
                    forcePage={props.currentPage}
                    onPageChange={page => props.handlePagination(page)}
                    pageCount={props.totalPages ?? 10}
                    breakLabel={'...'}
                    pageRangeDisplayed={props.pageRangeDisplayed ?? 2}
                    marginPagesDisplayed={props.marginPagesDisplayed ?? 2}
                    activeClassName='active'
                    pageClassName='page-item'
                    breakClassName='page-item'
                    nextLinkClassName='page-link'
                    pageLinkClassName='page-link'
                    breakLinkClassName='page-link'
                    previousLinkClassName='page-link'
                    nextClassName='page-item next-item'
                    previousClassName='page-item prev-item'
                    containerClassName={'pagination react-paginate separated-pagination pagination-sm justify-content-end pe-1'}
                />
            </Col>
        </Row>
    </div>
)
