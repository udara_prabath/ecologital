import React from "react"
import {Button, Col, Input, Label, Row} from "reactstrap"
import {FILTER_TYPES, FILTERS} from '../../const'
import {X} from "react-feather"
import './style.scss'

class App extends React.Component {

    state = {
        isAdvancedFilter: false
    }

    async UNSAFE_componentWillMount() {
        await this.initState()
    }

    initState = async () => {
        const data = {}
        this.props.list.map(item => {
            if (item.type === FILTER_TYPES.input || item.type === FILTER_TYPES.number) data[item.name] = item.value ?? ''
            if (item.type === FILTER_TYPES.rangePicker || item.type === FILTER_TYPES.dropDown || item.type === FILTER_TYPES.timeRangePicker) data[item.name] = item.value ?? null
        })
        await this.setState({...data})
    }

    onChange = async (e) => {
        await this.setState({[e.target.name]: e.target.value})
    }

    clearFilter = async () => {
        this.props.onFilter(FILTERS, true)
        const data = {}
        this.props.list.map(item => {
            if ((item.type === FILTER_TYPES.input || item.type === FILTER_TYPES.number) && !item.readOnly) data[item.name] = ''
            if (item.type === FILTER_TYPES.rangePicker || item.type === FILTER_TYPES.dropDown || item.type === FILTER_TYPES.timeRangePicker) data[item.name] = item.select ?? null
        })
        await this.setState({...data})
    }

    render() {
        const {list, onFilter} = this.props
        const state = this.state
        const length = list.length % 4
        const col = length === 0 ? 12 : length === 1 ? 9 : length === 2 ? 6 : 3

        return state && <Row className='mt-1 mb-50'>
            {
                list.map((item, index) => {
                    return <Col sm={3}>
                        <div className={'mb-1'}>
                            <Label for={item.name}>{item.label}</Label>

                            {
                                item.type === FILTER_TYPES.input &&
                                <Input
                                    id={item.name}
                                    placeholder={item.placeholder}
                                    onChange={this.onChange}
                                    onKeyDown={(e) => e.keyCode === 13 && onFilter(state)}
                                    name={item.name}
                                    readOnly={item.readOnly}
                                    value={state[item.name]}
                                />
                            }

                            {
                                item.type === FILTER_TYPES.number &&
                                <Input
                                    id={item.name}
                                    placeholder={item.placeholder}
                                    onChange={this.onChange}
                                    onKeyDown={(e) => e.keyCode === 13 && onFilter(state)}
                                    name={item.name}
                                    type='number'
                                    value={state[item.name]}
                                />
                            }
                        </div>
                    </Col>
                })
            }

            <Col sm={col} align={'right'}>
                <Button onClick={this.clearFilter} className='custom-btn-clear'>
                    <X size={15}/>
                    <span className='align-middle ml-50'> Clear</span>
                </Button>

                <Button onClick={() => onFilter(state)} className='custom-btn mt-2' color='primary'>
                    <span className='align-middle ml-50'> Filter</span>
                </Button>
            </Col>
        </Row>
    }
}

export default App
