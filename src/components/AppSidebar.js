import React from 'react'

import {CSidebar, CSidebarBrand, CSidebarNav} from '@coreui/react'

import {AppSidebarNav} from './AppSidebarNav'


import SimpleBar from 'simplebar-react'
import 'simplebar/dist/simplebar.min.css'
import logo from '../assets/logo.png'

// sidebar nav config
import navigation, {_navUser} from '../_nav'
import Cookies from "js-cookie";

const AppSidebar = () => {
    const admin = Cookies.get("USER_ROLE") === 'ADMIN'
    return (
        <CSidebar
            position="fixed"
            unfoldable={false}
            visible={true}
        >
            <CSidebarBrand className="d-none d-md-flex" to="/">
                <img src={logo} alt='logo' />
            </CSidebarBrand>
            <CSidebarNav>
                <SimpleBar>
                    <AppSidebarNav items={admin ? navigation : _navUser}/>
                </SimpleBar>
            </CSidebarNav>
        </CSidebar>
    )
}

export default React.memo(AppSidebar)
