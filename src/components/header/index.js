import React from 'react'
import {CAvatar, CDropdown, CDropdownItem, CDropdownMenu, CDropdownToggle,} from '@coreui/react'
import {cilLockLocked, cilUser,} from '@coreui/icons'
import CIcon from '@coreui/icons-react'
import Cookies from "js-cookie";

import avatar8 from './../../assets/avatar.jpg'

const AppHeaderDropdown = () => {
    const logout = () => {
        Cookies.remove("USER")
        Cookies.remove("USER_ROLE")
        Cookies.remove("USER_ID")
        Cookies.remove("ACCESS_TOKEN")
        window.open('/', '_self')
    }

    return (
        <CDropdown variant="nav-item">
            <CDropdownToggle placement="bottom-end" className="py-0" caret={false}>
                <CAvatar src={avatar8} size="md"/>
            </CDropdownToggle>
            <CDropdownMenu className="pt-0" placement="bottom-end">
                <CDropdownItem href="/profile">
                    <CIcon icon={cilUser} className="me-2"/>
                    Profile
                </CDropdownItem>
                <CDropdownItem onClick={logout}>
                    <CIcon icon={cilLockLocked} className="me-2"/>
                    Log Out
                </CDropdownItem>
            </CDropdownMenu>
        </CDropdown>
    )
}

export default AppHeaderDropdown
