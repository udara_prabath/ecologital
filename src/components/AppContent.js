import React, {Suspense} from 'react'
import {Navigate, Route, Routes} from 'react-router-dom'
import {CContainer, CSpinner} from '@coreui/react'
import {routes as rs} from "../router/const";
// routes config
import routes, {routesUser} from '../routes'
import Cookies from "js-cookie"

const AppContent = () => {
    const FinalRoute = () => {
        const user = Cookies.get("USER")
        const userRole = Cookies.get("USER_ROLE")
        if (user) {
            if (userRole === "ADMIN") {
                return <Navigate to={rs.users}/>
            } else {
                return <Navigate to={rs.profile}/>
            }
        } else {
            return <Navigate to={rs.login}/>
        }
    }

    const getRoutes = () => {
        const userRole = Cookies.get("USER_ROLE")
        if (userRole === "ADMIN") {
            return routes
        } else {
            return routesUser
        }
    }

    return (
        <CContainer xxl>
            <Suspense fallback={<CSpinner color="primary"/>}>
                <Routes>
                    {getRoutes().map((route, idx) => {
                        return (
                            route.element && (
                                <Route
                                    key={idx}
                                    path={route.path}
                                    exact={route.exact}
                                    name={route.name}
                                    element={<route.element/>}
                                />
                            )
                        )
                    })}
                    <Route path="/" element={<FinalRoute/>}/>
                </Routes>
            </Suspense>
        </CContainer>
    )
}

export default React.memo(AppContent)
