import {Modal, ModalHeader, ModalBody, Button, ModalFooter} from "reactstrap"

export default (props) => {
    return <Modal
        isOpen={props.isOpen}
        toggle={props.toggleModal}
        className={`modal-dialog-centered `}
    >
        <ModalHeader toggle={props.toggleModal}>
            {props.title}
        </ModalHeader>
        <ModalBody>
            <div className="d-flex flex-row align-items-center">
                {props.icon}
                <p className="confirm-msg">{props.message}</p>
            </div>
        </ModalBody>
        <ModalFooter>
            <Button color='primary' onClick={() => props.yesBtnClick()} type='button' >
                {props.yesBtn ?? 'Yes'}
            </Button>
            {
                !props.singleBtn &&
                <Button className='me-1' outline color='primary' type='button' onClick={() => props.noBtnClick()}>
                    {props.noBtn ?? 'No'}
                </Button>
            }

        </ModalFooter>
    </Modal>
}
