import React from 'react'
import {CContainer, CHeader, CHeaderNav,} from '@coreui/react'
import AppHeaderDropdown from './header'

const AppHeader = () => {

    return (
        <CHeader position="sticky" className="mb-4">
            <CContainer fluid>
                <CHeaderNav className="ms-3">
                    <AppHeaderDropdown/>
                </CHeaderNav>
            </CContainer>
        </CHeader>
    )
}

export default AppHeader
