import React, {Suspense} from 'react'
import {BrowserRouter as AppRouter, Route, Routes} from 'react-router-dom'
import {routes} from "./const";
import Login from "../pages/login";
import {ToastContainer} from "react-toastify";

const DefaultLayout = React.lazy(() => import('../layout/DefaultLayout'))

export default () => {

    const loading = (
        <div className="pt-3 text-center">
            <div className="sk-spinner sk-spinner-pulse"></div>
        </div>
    )

    return (
        <AppRouter basename={process.env.REACT_APP_BASENAME}>
            <Suspense fallback={loading}>
                <Routes>
                    <Route exact path={routes.login} element={<Login/>}/>
                    <Route exact path='*' element={<DefaultLayout/>}/>
                </Routes>
                <ToastContainer newestOnTop/>
            </Suspense>
        </AppRouter>
    )
}
