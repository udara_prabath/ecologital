export const routes = {
    login: '/login',
    register: '/register',
    home: '/',
    profile: '/profile',
    users: '/users'
}
