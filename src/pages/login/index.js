import React, {useState} from "react";
import './authentication.scss'
import {Alert, Button, CardText, CardTitle, Col, Form, FormGroup, Input, Label, Row} from 'reactstrap'
import logo from '../../assets/logo.png'
import loginBg from '../../assets/loginbg.svg'
import InputPasswordToggle from "../../components/input-password-toggle";
import {loginUser} from "../../service/authService";
import {routes} from "../../router/const";
import {useNavigate} from 'react-router-dom'
import Cookies from "js-cookie";

export default () => {
    const navigate = useNavigate();
    const [email, setEmail] = useState('admin@demo.com')
    const [password, setPassword] = useState('admin')

    const onSubmit = async (event) => {
        event.preventDefault()
        await loginUser({email, password})
            .then(res => {
                if (res.success) {
                    Cookies.set("USER", JSON.stringify(res.body.user))
                    Cookies.set("USER_ROLE", res.body.user.user_role)
                    Cookies.set("USER_ID", res.body.user._id)
                    Cookies.set("ACCESS_TOKEN", res.body.accessToken)
                    navigate(routes.home)
                }
            })
    }

    return <div className='auth-wrapper auth-cover'>
        <Row className='auth-inner m-0'>
            <Col lg='12' className={'logo-div'}>
                <img src={logo}/>
            </Col>
            <Col className='d-none d-lg-flex align-items-center p-5' lg='8' sm='12'>
                <div className='w-100 d-lg-flex align-items-center justify-content-center px-5'>
                    <img className='img-fluid' src={loginBg} alt='Login V2'/>
                </div>
            </Col>


            <Col className='d-flex align-items-center auth-bg px-2 p-lg-5 bg-white' lg='4' sm='12'>
                <Col className='px-xl-2 mx-auto' sm='8' md='6' lg='12'>
                    <CardTitle tag='h2' className='font-weight-bold mb-1'>
                        Welcome Back! 👋
                    </CardTitle>
                    <CardText className='mb-2'>Please sign-in to your account and start the adventure</CardText>
                    <Alert color='primary'>
                        <div className='alert-body font-small-2'>
                            <small className='mr-50'>
                                <span className='font-weight-bold'>Admin:</span> admin@demo.com | admin
                            </small>
                        </div>
                    </Alert>
                    <Form className='auth-login-form mt-2' onSubmit={onSubmit}>
                        <FormGroup>
                            <Label className='form-label' for='login-email'>
                                Email
                            </Label>
                            <Input
                                autoFocus
                                type='email'
                                value={email}
                                id='login-email'
                                name='login-email'
                                required
                                placeholder='john@example.com'
                                onChange={e => setEmail(e.target.value)}
                            />
                        </FormGroup>
                        <FormGroup>
                            <div className='d-flex justify-content-between'>
                                <Label className='form-label' for='login-password'>
                                    Password
                                </Label>
                            </div>
                            <InputPasswordToggle
                                value={password}
                                id='login-password'
                                required
                                name='login-password'
                                className='input-group-merge'
                                onChange={e => setPassword(e.target.value)}
                            />
                        </FormGroup>
                        <Button type='submit' color='primary' block>
                            Sign in
                        </Button>
                    </Form>
                </Col>
            </Col>
        </Row>
    </div>
}

