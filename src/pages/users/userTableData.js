import {Edit2, Eye, Trash} from "react-feather"
import {Input} from "reactstrap"

export const USER_PROFILE_TABLE_COLUMN = (onEdit, onCheck, onDelete, onView) => {
    return [
        {
            name: '',
            width: '80px',
            cell: (row, index) => {
                return <Input onClick={(e) => onCheck(row, index)} type={'checkbox'}
                                             checked={row.selected}/>
            }
        },
        {
            name: 'FIRST NAME',
            minWidth: '100px',
            selector: row => row.first_name
        },
        {
            name: 'LAST NAME',
            minWidth: '100px',
            selector: row => row.last_name
        },
        {
            name: 'EMAIL',
            minWidth: '100px',
            selector: row => row.email
        },
        {
            name: 'ACTION',
            minWidth: '100px',
            allowOverflow: true,
            cell: (row, index) => <div className="d-flex flex-row cursor-pointer">
                <Eye size={18} className="me-2" onClick={() => onView(row)}/>
                <Trash size={18} className="me-2" onClick={() => onDelete(row)}/>
                <Edit2 size={18} className="me-2" onClick={() => onEdit(row)}/>
            </div>
        }
    ]
}
