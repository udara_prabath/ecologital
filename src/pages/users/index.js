import React, {Fragment} from 'react'
import {Button, Card, CardBody, CardHeader, CardTitle} from 'reactstrap'
import {USER_PROFILE_TABLE_COLUMN} from './userTableData'
import DataTable from 'react-data-table-component'
import {FILTER_TYPES, FILTERS, USERS_CSV_HEADER} from '../../const'
import CustomPagination from "../../components/customPagination"
import {HelpCircle, Plus, Trash, Upload} from 'react-feather'
import Filter from "../../components/filter"
import {CSVLink} from "react-csv"
import {deleteUsers, getAllUsers} from "../../service/userService"
import {Slide, toast} from "react-toastify"
import ConfirmBox from "../../components/confirm-box"
import UserSideView from "./userSideView"
import {withRouter} from '../../components/RouteWrapper'

class UserProfiles extends React.Component {
    csvLinkEl = React.createRef()
    state = {
        data: [],
        currentPage: 0,
        totalPages: 1,
        totalElements: 1,
        offset: 0,
        delete: false,
        update: false,
        filter: FILTERS,
        user: undefined,
        numberOfElements: 0,
        selectedRows: [],
        exportList: []
    }

    componentWillMount() {
        this.loadTableData(this.state.filter, 0)
    }

    loadTableData = async (data, page) => {
        let url = `users?page=${page}`
        if (data.firstName && data.firstName.trim() !== "") url += `&first_name=${data.firstName}`
        if (data.email && data.email.trim() !== "") url += `&email=${data.email}`
        if (data.lastName && data.lastName.trim() !== "") url += `&last_name=${data.lastName}`

        await getAllUsers(url)
            .then(res => {
                if (res.success) {
                    this.setState({
                        data: res.body.docs,
                        numberOfElements: 10,
                        totalElements: res.body.totalDocs,
                        totalPages: res.body.totalPages,
                        offset: 10 * this.state.currentPage,
                        pageSize: 10
                    })
                } else {
                    toast.error('Something went wrong!', {transition: Slide, hideProgressBar: true, autoClose: 3000})
                }
            })
    }

    onFilterHandler = async (data) => {
        await this.setState({currentPage: 0, filter: {...data}})
        this.loadTableData(data, 0)
    }

    handlePagination = async (val) => {
        await this.loadTableData(this.state.filter, val.selected)
        this.setState({
            currentPage: (val.selected)
        })
    }

    viewUser = (row, index) => {
        this.setState({user: row, update: true, delete: false})
    }

    onView = (row) => {
        this.props.navigate(`/users/${row._id}`)
    }

    onDelete = (row, index) => {
        this.setState({selectedRows: [row], delete: true, update: false})
    }

    onCheck = (item, index) => {
        const rows = [...this.state.data]
        let selectedRows = [...this.state.selectedRows]
        rows[index].selected = !rows[index].selected

        if (selectedRows.length === 0) {
            selectedRows.push(item)
        } else {
            if (rows[index].selected) {
                const found = selectedRows.filter(itm => itm.id !== item.id)
                if (found.length > 0) {
                    selectedRows.push(item)
                }
            } else {
                selectedRows = selectedRows.filter(selectedModule => item.id !== selectedModule.id)
            }
        }

        this.setState({data: rows, selectedRows})
    }

    exportUsers = async () => {
        await getAllUsers('users')
            .then(res => {
                if (res.success) {
                    if (res.body.length !== 0) {
                        this.setState({exportList: res.body})
                        this.csvLinkEl.current.link.click()
                    }
                } else {
                    toast.error('Something went wrong!', {transition: Slide, hideProgressBar: true, autoClose: 3000})
                }
            })
    }

    deleteUsers = async () => {
        await deleteUsers({
            userIds: this.state.selectedRows.map(item => {
                return item._id
            })
        })
            .then(async res => {
                if (res.success) {
                    toast.success('Operation Success!', {transition: Slide, hideProgressBar: true, autoClose: 3000})
                    await this.setState({delete: false, update: false, selectedRows: []})
                    await this.loadTableData(this.state.filter, this.state.currentPage)
                } else {
                    toast.error('Something went wrong!', {transition: Slide, hideProgressBar: true, autoClose: 3000})
                }
            })

    }

    render() {
        const {currentPage, numberOfElements, totalElements, totalPages, offset} = this.state

        return <Fragment>
            <Card>
                <CardHeader className='border-bottom'>
                    <CardTitle tag='h4' className={'heading'}>All Users</CardTitle>
                    <div className='d-flex mt-md-0 mt-1'>
                        <Button className={'top-custom-btn'} color='primary' outline size={'sm'}
                                onClick={() => this.exportUsers()}>
                            <Upload size={15}/>
                            <span className='align-middle ml-50'> Export </span>
                        </Button>
                        <CSVLink data={this.state.exportList} filename={"users.csv"}
                                 headers={USERS_CSV_HEADER} ref={this.csvLinkEl}>
                        </CSVLink>
                        <Button className='top-custom-btn ms-2' size="sm"
                                onClick={() => this.setState({user: undefined, update: true, delete: false})}
                                color='primary'>
                            <Plus size={15}/>
                            <span className='align-middle ml-50'> Add User</span>
                        </Button>
                    </div>
                </CardHeader>
                <CardBody>
                    <Filter
                        list={[
                            {
                                type: FILTER_TYPES.input,
                                name: 'firstName',
                                label: 'First Name',
                                placeholder: 'Search by first name',
                                value: this.state.filter.firstName
                            },
                            {
                                type: FILTER_TYPES.input,
                                name: 'lastName',
                                label: 'Last Name',
                                placeholder: 'Search by last name',
                                value: this.state.filter.lastName
                            },
                            {
                                type: FILTER_TYPES.input,
                                name: 'email',
                                label: 'Email',
                                placeholder: 'Search by email',
                                value: this.state.filter.email
                            }
                        ]}
                        onFilter={this.onFilterHandler}
                    />

                    {
                        this.state.selectedRows.length > 0 &&
                        <div className='d-flex justify-content-end mt-md-0 mt-4 mb-2 w-100'>
                            <Button className='top-custom-btn ms-2' size="sm"
                                    onClick={() => this.setState({delete: true, update: false})}
                                    color='primary'>
                                <Trash size={15}/>
                                <span className='align-middle ml-50'> Delete Users</span>
                            </Button>
                        </div>
                    }

                    <div className='react-dataTable mt-3'>
                        <DataTable
                            noHeader
                            pagination
                            data={this.state.data}
                            columns={USER_PROFILE_TABLE_COLUMN(this.viewUser, this.onCheck, this.onDelete, this.onView)}
                            className='react-dataTable'
                            paginationDefaultPage={this.state.currentPage + 1}
                            paginationRowsPerPageOptions={[10, 25, 50, 100]}
                            paginationComponent={() => CustomPagination({
                                currentPage,
                                numberOfElements,
                                totalElements,
                                totalPages,
                                offset,
                                handlePagination: page => this.handlePagination(page)
                            })}
                        />
                    </div>
                </CardBody>
            </Card>

            <ConfirmBox
                isOpen={this.state.delete}
                toggleModal={() => this.setState({delete: false, update: false})}
                yesBtnClick={this.deleteUsers}
                noBtnClick={() => {
                    this.setState({delete: false, update: false})
                }}
                title={'Confirmation'}
                message={`Do you want to delete user(s)?`}
                yesBtn="Yes"
                noBtn="No"
                icon={<HelpCircle size={40} color="#29AC8D"/>}
            />

            {
                this.state.update &&
                <UserSideView
                    open={this.state.update}
                    user={this.state.user}
                    afterSave={() => this.loadTableData(this.state.filter, this.state.currentPage)}
                    toggleOpen={() => this.setState({user: undefined, update: false, delete: false})}
                />
            }
        </Fragment>
    }
}

export default withRouter(UserProfiles)
