import {
    Button,
    CardTitle,
    Col,
    Form,
    FormGroup,
    Input,
    Label,
    Offcanvas,
    OffcanvasBody,
    OffcanvasHeader,
    Row
} from 'reactstrap'
import {FileText} from "react-feather"
import Required from "../../components/required"
import React, {useEffect, useState} from "react"
import InputPasswordToggle from '../../components/input-password-toggle'
import {saveUser} from "../../service/userService"
import {Slide, toast} from "react-toastify"

export default (props) => {
    const {open, toggleOpen, user, afterSave} = props
    const [firstName, setFirstName] = useState('')
    const [lastName, setLastName] = useState('')
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')

    useEffect(() => {
        if (user) {
            setFirstName(user.first_name)
            setLastName(user.last_name)
            setEmail(user.email)
            setPassword(user.password)
        }
    }, [])

    const onSubmit = async data => {
        data.preventDefault()
        await saveUser({
            id: user ? user._id : undefined,
            email: user ? (user.email !== email ? email : user.email) : email,
            newEmail: user ? (user.email !== email) : true,
            first_name: firstName,
            last_name: lastName,
            password,
        }).then(res => {
            if (res.success) {
                toast.success('Operation Success!', {transition: Slide, hideProgressBar: true, autoClose: 3000})
                afterSave()
                toggleOpen()
            }
        })
            .catch(error => {
                toast.error(
                    'Something went wrong!',
                    {transition: Slide, hideProgressBar: true, autoClose: 3000}
                )
            })
    }

    return <Offcanvas
        scrollable={true}
        backdrop={true}
        direction='end'
        isOpen={open}
        toggle={toggleOpen}
    >
        <OffcanvasHeader toggle={toggleOpen}>{user ? 'Edit' : 'Add'} User</OffcanvasHeader>
        <OffcanvasBody className='mx-0 flex-grow-0 min-h-100'>
            <Row>
                <CardTitle tag='h4' className="mt-1"><FileText size={20}/> &nbsp;User Details</CardTitle>

                <Form className='auth-login-form mt-2' onSubmit={onSubmit}>
                    <FormGroup>
                        <Label className='form-label' for='login-email'>
                            First Name<Required/>
                        </Label>
                        <Input
                            autoFocus
                            type='text'
                            value={firstName}
                            required
                            placeholder='Enter first name'
                            onChange={e => setFirstName(e.target.value)}
                        />
                    </FormGroup>
                    <FormGroup>
                        <Label className='form-label' for='login-email'>
                            Last Name<Required/>
                        </Label>
                        <Input
                            autoFocus
                            type='text'
                            value={lastName}
                            required
                            placeholder='Enter last name'
                            onChange={e => setLastName(e.target.value)}
                        />
                    </FormGroup>
                    <FormGroup>
                        <Label className='form-label' for='login-email'>
                            Email<Required/>
                        </Label>
                        <Input
                            autoFocus
                            type='email'
                            value={email}
                            id='login-email'
                            name='login-email'
                            required
                            placeholder='john@example.com'
                            onChange={e => setEmail(e.target.value)}
                            // className={classnames({ 'is-invalid': errors['login-email'] })}
                        />
                    </FormGroup>
                    <FormGroup>
                        <div className='d-flex justify-content-between'>
                            <Label className='form-label' for='login-password'>
                                Password<Required/>
                            </Label>
                        </div>
                        <InputPasswordToggle
                            value={password}
                            id='login-password'
                            required
                            name='login-password'
                            className='input-group-merge'
                            onChange={e => setPassword(e.target.value)}
                            // className={classnames({ 'is-invalid': errors['login-password'] })}
                        />
                    </FormGroup>
                    <Col sm='12'>
                        <div className='d-flex mt-3 justify-content-end pb-3'>
                            <Button className='me-1' outline color='primary' type='button' onClick={toggleOpen}>
                                Cancel
                            </Button>
                            <Button color='primary' type='submit'>
                                {user ? "Update" : "Add"}
                            </Button>
                        </div>
                    </Col>
                </Form>
            </Row>
        </OffcanvasBody>
    </Offcanvas>
}
