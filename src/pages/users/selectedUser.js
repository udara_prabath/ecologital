import React, {useEffect, useState} from 'react'
import {Button, Card, CardBody, Col, Form, FormGroup, Input, Label, Row} from "reactstrap"
import {HelpCircle, User} from "react-feather"
import {getUser, saveUser} from "../../service/userService"
import {Slide, toast} from "react-toastify"
import ConfirmBox from "../../components/confirm-box"
import {withRouter} from '../../components/RouteWrapper'
import {useParams} from "react-router-dom";

const SelectedUser = (props) => {
    const [firstName, setFirstName] = useState('')
    const [lastName, setLastName] = useState('')
    const [email, setEmail] = useState('')
    const [update, setUpdate] = useState(false)
    let {userId} = useParams();

    useEffect(() => {
        getUserDetails()
    }, [])

    const getUserDetails = async () => {
        await getUser(userId ?? 0)
            .then(async res => {
                if (res.success) {
                    const user = res.body
                    setFirstName(user.first_name)
                    setLastName(user.last_name)
                    setEmail(user.email)
                } else {
                    toast.error('Something went wrong!', {transition: Slide, hideProgressBar: true, autoClose: 3000})
                }
            })
    }

    const updateUser = async () => {
        await saveUser({
            id: userId,
            email,
            first_name: firstName,
            last_name: lastName
        }).then(res => {
            toast.success(
                'Operation Success!',
                {transition: Slide, hideProgressBar: true, autoClose: 3000}
            )
            setUpdate(false)
        })
            .catch(error => {
                toast.error(
                    'Something went wrong!',
                    {transition: Slide, hideProgressBar: true, autoClose: 3000}
                )
                setUpdate(false)
            })
    }

    return (
        <Card>
            <CardBody>
                <div className={'heading'}>
                    <User id={'icon'}/> Personal Information
                </div>
                <Form className='auth-login-form mt-2' onSubmit={e => {
                    e.preventDefault()
                    setUpdate(!update)
                }}>
                    <Row>
                        <Col xs={6}>
                            <FormGroup>
                                <Label className='form-label' for='login-email'>
                                    First Name
                                </Label>
                                <Input
                                    autoFocus
                                    type='text'
                                    value={firstName}
                                    required
                                    placeholder='Enter first name'
                                    onChange={e => setFirstName(e.target.value)}
                                />
                            </FormGroup>
                        </Col>
                        <Col xs={6}>
                            <FormGroup>
                                <Label className='form-label' for='login-email'>
                                    Last Name
                                </Label>
                                <Input
                                    autoFocus
                                    type='text'
                                    value={lastName}
                                    required
                                    placeholder='Enter last name'
                                    onChange={e => setLastName(e.target.value)}
                                />
                            </FormGroup>
                        </Col>
                        <Col xs={6}>
                            <FormGroup>
                                <Label className='form-label' for='login-email'>
                                    Email
                                </Label>
                                <Input
                                    autoFocus
                                    type='email'
                                    value={email}
                                    id='login-email'
                                    name='login-email'
                                    required
                                    placeholder='john@example.com'
                                    onChange={e => setEmail(e.target.value)}
                                    // className={classnames({ 'is-invalid': errors['login-email'] })}
                                />
                            </FormGroup>
                        </Col>
                        <Col xs={6}>
                            <Label className='form-label' for='login-email'>

                            </Label>
                            <div className='d-flex justify-content-end pb-3'>
                                <Button className='me-1' outline color='primary' type='button'
                                        onClick={() => props.navigate('/')}>
                                    Cancel
                                </Button>
                                <Button color='primary' type='submit'>
                                    Update User
                                </Button>
                            </div>
                        </Col>
                    </Row>
                </Form>

                <ConfirmBox
                    isOpen={update}
                    toggleModal={() => setUpdate(false)}
                    yesBtnClick={updateUser}
                    noBtnClick={() => {
                        setUpdate(false)
                    }}
                    title={'Confirmation'}
                    message={`Do you want to update user?`}
                    yesBtn="Yes"
                    noBtn="No"
                    icon={<HelpCircle size={40} color="#29AC8D"/>}
                />
            </CardBody>
        </Card>
    )
}

export default withRouter(SelectedUser)
