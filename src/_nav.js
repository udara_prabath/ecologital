import React from 'react'
import { CNavItem } from '@coreui/react'
import CIcon from "@coreui/icons-react";
import {cilUser, cilPencil} from "@coreui/icons";

const _nav = [
  {
    component: CNavItem,
    name: 'Users',
    to: '/users',
    icon:  <CIcon icon={ cilUser} customClassName="nav-icon"/>,
  },
  {
    component: CNavItem,
    name: 'Profile',
    to: '/profile',
    icon:  <CIcon icon={ cilPencil} customClassName="nav-icon"/>,
  }
]

export const _navUser = [
  {
    component: CNavItem,
    name: 'Profile',
    to: '/profile',
    icon:  <CIcon icon={ cilPencil} customClassName="nav-icon"/>,
  }
]

export default _nav
