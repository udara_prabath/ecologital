import ApiService from './apiHandler'

export async function loginUser(userCredentials) {
    const apiObject = {}
    apiObject.method = 'POST'
    apiObject.authentication = false
    apiObject.isBasicAuth = true
    apiObject.endpoint = 'oauth/token'
    apiObject.body = userCredentials
    apiObject.state = "login"
    return await ApiService.callApi(apiObject)
}
