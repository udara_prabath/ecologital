const conf = {
    // serverUrl: 'https://959a32bb-ace7-40c8-8f63-90feeeb5b39e.mock.pstmn.io',
    serverUrl: 'http://localhost:5000',
    basePath: `api`,
    version: `v1/`
}

export default conf
