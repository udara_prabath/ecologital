export default {
    accessTokenKeyName: 'ACCESS_TOKEN',
    refreshTokenKeyName: 'REFRESH_TOKEN',
    basicAuthString: 'ZWNvbG9naXRhbDpiYWNrZW5k'
}
