import axios from 'axios'
import ApiService from "./apiHandler";


export async function getAllUsers(url) {
    const apiObject = {}
    apiObject.method = 'GET'
    apiObject.authentication = true
    apiObject.endpoint = url
    return await ApiService.callApi(apiObject)
}

export async function saveUser(data) {
    const apiObject = {}
    apiObject.method = 'POST'
    apiObject.authentication = true
    apiObject.endpoint = 'users'
    apiObject.body = data
    return await ApiService.callApi(apiObject)
}

export async function deleteUsers(data) {
    const apiObject = {}
    apiObject.method = 'POST'
    apiObject.authentication = true
    apiObject.endpoint = 'users/delete'
    apiObject.body = data
    return await ApiService.callApi(apiObject)
}
export async function getUser(userId) {
    const apiObject = {}
    apiObject.method = 'GET'
    apiObject.authentication = true
    apiObject.endpoint = `users/${userId}`
    return await ApiService.callApi(apiObject)
}

export default class UserService {
    getAllUsers(params) {
        return axios.get('/api/users', params)
    }

    deleteUsers(data) {
        return axios.post('/api/users/delete', data)
    }

    saveUser(data) {
        return axios.post('/api/users', data)
    }

    getSelectedUser(id) {
        return axios.get('/api/users/user', {id})
    }
}
