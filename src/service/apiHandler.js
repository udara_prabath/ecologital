import axios from 'axios'
import apiConfig from './apiConfig'
import Cookies from "js-cookie"
import config from './storageStrings'
import $ from 'jquery'
import {toast} from "react-toastify"

export const callApi = async(apiObject) => {
    apiObject.loading === undefined && $(".loadingEffect").css("display", "block")
    let body = {}
    const method = apiObject.method?.toLowerCase() ?? 'get'
    const headers = {
        'Content-Type': 'application/json'
    }

    if (method === 'post' || method === 'put' || method === 'patch') {
        body = apiObject.body ?? {}
    }

    if (apiObject.authentication) {
        const access_token = Cookies.get(config.accessTokenKeyName)
        if (access_token) {
            headers.Authorization = `Bearer ${access_token}`
        }
    }

    if (apiObject.isBasicAuth) {
        headers.Authorization = `Basic ${config.basicAuthString}`
    }

    const url = `${apiConfig.serverUrl}/${apiConfig.basePath}/${(apiObject.state !== "renewToken" && apiObject.state !== "login") ? apiConfig.version : "" }${apiObject.endpoint}`
    let result
    await axios[method](url, (method !== 'get' && method !== 'delete') ? body : {headers}, {headers})
        .then(response => {
            $(".loadingEffect").css("display", "none")
            result = response.data
            if (!result.success) {
                toast.error(result.message, {icon: true, hideProgressBar: true})
            }
        })
        .catch(async error => {
            $(".loadingEffect").css("display", "none")
            if (error?.response === undefined) {
                result = {success: false, msg: "Your connection was interrupted"}
            }

            if (error?.response?.status === 401) {
                if (apiObject.state === "login") {
                    result = {success: false, msg: error?.response?.data?.msg ?? "Invalid login details please try again!"}
                }

                if (error?.response?.data?.message  && (error.response.data.message === 'User is not active')) {
                    result = {success: false, status:1, message: "User is not active."}
                }
                // result = await renewTokenHandler(apiObject)
            } else if (error?.response?.data) {
                result = {
                    success: false,
                    msg: error.response.data.message
                }
            } else {
                result = {
                    success: false,
                    msg: "Your connection was interrupted!"
                }
            }
            if (!result?.success) {
                toast.error(result.msg, {icon: true, hideProgressBar: true})
            }
        })
    return result

}


export default {callApi}
