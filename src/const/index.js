
export const DATE_RANGE_FORMAT = 'd/m/Y'
export const FILTERS = {
    firstName: undefined,
    lastName: undefined,
    id: undefined,
    email: undefined
}

export const FILTER_TYPES = {
    input: 'INPUT',
    dropDown: 'DROP_DOWN',
    datePicker: 'DATE_PICKER',
    rangePicker: 'RANGE_PICKER',
    number: 'NUMBER',
    timeRangePicker: 'TIME_RANGE_PICKER'
}

export const USERS_CSV_HEADER = [
    {label: "User Id", key: "_id"},
    {label: "First Name", key: "first_name"},
    {label: "last Name", key: "last_name"},
    {label: "Email", key: "email"}
]
